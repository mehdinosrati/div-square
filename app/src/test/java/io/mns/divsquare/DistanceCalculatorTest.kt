package io.mns.divsquare

import io.mns.divsquare.data.model.GPSLocation
import io.mns.divsquare.data.model.minus
import org.junit.Test

class DistanceCalculatorTest {

    @Test
    fun `calculate distance between 2 known places`() {
        // university to dormitory air distance
        val loc1 = GPSLocation(latitude = 35.704670,longitude = 51.412357)
        val loc2 = GPSLocation(latitude = 35.710141,longitude = 51.411724)
        assert(loc2 - loc1 == loc1 - loc2) {
            "different values for same spots"
        }
        assert(loc2 - loc1 in 601..619)
    }
}