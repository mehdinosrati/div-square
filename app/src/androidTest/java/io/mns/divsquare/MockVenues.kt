package io.mns.divsquare

import io.mns.divsquare.data.model.Location
import io.mns.divsquare.data.model.Venue
import java.util.*

val sampleVenues = listOf(
    Venue(
        id = UUID.randomUUID().toString(),
        name = "test venue 1",
        verified = false,
        icon = null,
        location = Location(
            address = "test address 1",
            latitude = 12.2,
            longitude = 45.6,
            distance = 450,
            crossStreet = null,
            city = null
        )
    ),
    Venue(
        id = UUID.randomUUID().toString(),
        name = "test venue 2",
        verified = false,
        icon = null,
        location = Location(
            address = "test address 2",
            latitude = 42.2,
            longitude = 47.6,
            distance = 490,
            crossStreet = null,
            city = null
        )
    ),
    Venue(
        id = UUID.randomUUID().toString(),
        name = "test venue 3",
        verified = false,
        icon = null,
        location = Location(
            address = "test address 3",
            latitude = 72.2,
            longitude = 45.6,
            distance = 510,
            crossStreet = null,
            city = null
        )
    )
)
