package io.mns.divsquare

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mns.divsquare.data.cache.VenueDataBase
import io.mns.divsquare.data.cache.daos.VenueDAO
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class VenueDataBaseTest {
    private lateinit var dao: VenueDAO
    private lateinit var dataBase: VenueDataBase

    @Before
    fun createDataBase() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        dataBase = Room.inMemoryDatabaseBuilder(
            context, VenueDataBase::class.java
        ).build()
        dao = dataBase.venueDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDataBase() {
        dataBase.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndQueryVenues_shouldReturnAnIdenticalCollectionAsInsertedOne() = runBlocking {
        dao.insertVenues(sampleVenues)
        val fromDB = dao.suspendLoadVenues()
        for (venue in sampleVenues) {
            assertEquals(fromDB[sampleVenues.indexOf(venue)], venue)
        }
    }

    @Test
    @Throws(Exception::class)
    fun venuesCountInDB_countShouldBeSameAsTheInsertedCollectionSize() = runBlocking {
        dao.insertVenues(sampleVenues)
        val count = dao.getVenueCount()
        assertEquals(count, sampleVenues.size)
    }

    @Test
    @Throws(Exception::class)
    fun invalidateVenues_countShouldBeZeroAfterInvalidation() = runBlocking {
        dao.insertVenues(sampleVenues)
        dao.invalidateVenues()
        val count = dao.getVenueCount()
        assertEquals(count, 0)
    }

    @Test
    @Throws(Exception::class)
    fun getVenueById_shouldReturnTheVenueWithGivenId() = runBlocking {
        dao.insertVenues(sampleVenues)
        val venue = dao.getVenueById(sampleVenues[1].id)
        assertEquals(venue, sampleVenues[1])
    }

}