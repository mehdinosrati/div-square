package io.mns.divsquare.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import io.mns.divsquare.data.cache.daos.VenueDAO
import io.mns.divsquare.data.model.Venue

/**
 * Room Database definition, uses given entities to create needed tables
 * Version specifies schema state and would be helpful with migrations
 */
@Database(version = 1, entities = [Venue::class], exportSchema = false)
abstract class VenueDataBase : RoomDatabase() {

    /**
     * @see io.mns.divsquare.data.cache.daos.VenueDAO
     */
    abstract fun venueDao(): VenueDAO
}