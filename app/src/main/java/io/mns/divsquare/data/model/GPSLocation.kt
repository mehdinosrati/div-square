package io.mns.divsquare.data.model

import androidx.annotation.Keep
import io.mns.divsquare.EARTH_RADIUS
import java.lang.Math.toRadians
import kotlin.math.*

@Keep
data class GPSLocation(
    val latitude: Double,
    val longitude: Double
)

/**
 * calculates distance between two given locations based
 * on their longitude and latitude, using Haversin formula.
 */
operator fun GPSLocation.minus(otherGPSLocation: GPSLocation): Int {
    val lambda1 = toRadians(latitude)
    val lambda2 = toRadians(otherGPSLocation.latitude)
    val deltaLambda = toRadians(otherGPSLocation.latitude - latitude)
    val deltaPhi = toRadians(otherGPSLocation.longitude - longitude)
    return (2 * EARTH_RADIUS * asin(
        sqrt(
            sin(deltaLambda / 2).pow(2.0) + sin(deltaPhi / 2).pow(2.0) * cos(
                lambda1
            ) * cos(lambda2)
        )
    )).toInt()
}