package io.mns.divsquare.data.model

import androidx.annotation.Keep

/**
 * data class to load back the base info about last session
 */
@Keep
data class VenueAppState(
    val lastGPSLocation: GPSLocation,
    val lastKnownAreaName: String,
    val lastVenueCount: Int
)