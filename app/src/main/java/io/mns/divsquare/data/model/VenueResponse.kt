package io.mns.divsquare.data.model

import androidx.annotation.Keep
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Keep
@Entity(tableName = "venues")
data class Venue(
    @PrimaryKey
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @Embedded
    @SerializedName("location") val location: Location,
    @SerializedName("verified") val verified: Boolean,
    val icon: String?
) {
    @Ignore
    @SerializedName("categories")
    var categories: List<Category>? = null

    /**
     * In order to pull out the Icon from the category
     * collection of venue, and no to mutate it, constructor
     * gets a venue with no icon, and a separate icon url
     * @param icon url to be included with the Venue object
     */
    constructor(venue: Venue, icon: String?) : this(
        id = venue.id,
        name = venue.name,
        location = venue.location,
        verified = venue.verified,
        icon = icon
    )
}

@Keep
data class VenueResponse(
    @SerializedName("response") val response: Response
)

@Keep
data class Response(
    @SerializedName("headerFullLocation") val areaName: String,
    @SerializedName("totalResults") val totalResults: Int,
    @SerializedName("groups") val groups: List<Groups>
)

@Keep
data class Groups(
    @SerializedName("items") val items: List<Items>
)

@Keep
data class Items(
    @SerializedName("venue") val venue: Venue
)

@Keep
data class Category(
    @SerializedName("name") val name: String,
    @SerializedName("icon") val icon: Icon
)

@Keep
data class Icon(
    @SerializedName("prefix") val path: String,
    @SerializedName("suffix") val extension: String
)

@Keep
data class Location(
    @SerializedName("address") val address: String?,
    @SerializedName("crossStreet") val crossStreet: String?,
    @SerializedName("lat") val latitude: Double,
    @SerializedName("lng") val longitude: Double,
    @SerializedName("distance") val distance: Int,
    @SerializedName("city") val city: String?
)
