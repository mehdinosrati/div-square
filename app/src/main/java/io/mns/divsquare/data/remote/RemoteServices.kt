package io.mns.divsquare.data.remote

import io.mns.divsquare.data.model.VenueResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit service definition file including
 * configuration for various endpoint(s)
 */
interface RemoteServices {

    /**
     * Fetches the Venues around, sorted by distance.
     * Uses client ID and secret for anonymous authentication
     */
    @GET("venues/explore")
    suspend fun getVenuesUserLess(
        @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String,
        @Query("ll") location: String,
        @Query("v") version: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("sortByDistance") sortByDistance: Int
    ): VenueResponse

}