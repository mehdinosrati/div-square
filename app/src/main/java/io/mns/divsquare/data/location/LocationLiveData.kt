package io.mns.divsquare.data.location

import android.content.Context
import android.location.Location
import androidx.lifecycle.LiveData
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import io.mns.divsquare.MINIMUM_UPDATE_DISTANCE
import io.mns.divsquare.data.model.GPSLocation

/**
 * Base class to find the location off the main thread,
 * update it continuously and observe it in lifecycle owners
 * on the main thread. Uses liveData and FusedLocationProviderClient}
 * to make it happen.
 * @param context to access the [FusedLocationProviderClient]
 */
class LocationLiveData(context: Context) : LiveData<GPSLocation>() {

    private var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    /**
     * Unregister [FusedLocationProviderClient] update callback,
     * cause no one wants to know about it.
     */
    override fun onInactive() {
        super.onInactive()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    /**
     * Someone is interested in location stuff,
     * give it latest known location and register
     * a periodic update request
     */
    override fun onActive() {
        super.onActive()
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.also {
                    setLocationData(it)
                }
            }
        startLocationUpdates()
    }

    /**
     * Checks for the current value, which came from [FusedLocationProviderClient.lastLocation],
     * if the value was null, registers one single shot request
     * to find the location, when it is done, registers the
     * regular update request, which is way less power consuming
     */
    private fun startLocationUpdates() {
        if (value == null) {
            fusedLocationClient.requestLocationUpdates(
                firstLaunchLocationRequest,
                locationCallback,
                null
            ).addOnCompleteListener {
                addRegularLocationListener()
            }
        } else {
            addRegularLocationListener()
        }
    }

    /**
     * @see [startLocationUpdates]
     */
    private fun addRegularLocationListener() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private val locationCallback = object : LocationCallback() {
        /**
         * When the gps finds a location data, it gets called.
         * Updates the location data of current liveData
         * @param locationResult that has been found
         */
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            for (location in locationResult.locations) {
                setLocationData(location)
            }
        }
    }

    private fun setLocationData(location: Location) {
        value = GPSLocation(
            longitude = location.longitude,
            latitude = location.latitude
        )
    }

    companion object {
        val locationRequest: LocationRequest = LocationRequest.create().apply {
            interval = 1000 * 60 * 10
            fastestInterval = 1000 * 10
            smallestDisplacement = MINIMUM_UPDATE_DISTANCE.toFloat()
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }

        val firstLaunchLocationRequest: LocationRequest = LocationRequest.create().apply {
            interval = 1000
            numUpdates = 1
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }
}