package io.mns.divsquare.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import io.mns.divsquare.BuildConfig
import io.mns.divsquare.MINIMUM_UPDATE_DISTANCE
import io.mns.divsquare.VENUE_ICON_SIZE
import io.mns.divsquare.VENUE_REMOTE_API_PAGE_SIZE
import io.mns.divsquare.data.cache.daos.VenueDAO
import io.mns.divsquare.data.model.*
import io.mns.divsquare.data.remote.RemoteServices
import io.mns.divsquare.utils.SharedPreferencesHelper
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Single source of truth for application.
 * Responsible for determining the data source.
 * Also holds data about application state
 * @param prefsHelper for state
 * @param remoteServices for remote data source
 * @param venueDAO for persistence (read / write)
 */
class VenuesRepository(
    private val remoteServices: RemoteServices,
    private val venueDAO: VenueDAO,
    private val prefsHelper: SharedPreferencesHelper
) {
    //region variable declaration
    private val repositoryScope = CoroutineScope(Dispatchers.IO)

    private var prevState = prefsHelper.getLastState()

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        _networkError.postValue(exception)
    }

    private var isLoadingSync = false

    private val _venues = MediatorLiveData<List<Venue>>()
    val venues: LiveData<List<Venue>>
        get() = _venues

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _networkConnected = MutableLiveData(false)
    val networkConnected: LiveData<Boolean>
        get() = _networkConnected

    private val _networkError = MutableLiveData<Throwable?>(null)
    val networkError: LiveData<Throwable?>
        get() = _networkError

    private val _hasLocationAccess = MutableLiveData(false)
    val hasLocationAccess: LiveData<Boolean>
        get() = _hasLocationAccess

    //endregion

    init {
        _venues.addSource(venueDAO.loadCachedVenues(), _venues::postValue)
    }

    //region location & network

    fun locationAccessGranted() {
        _hasLocationAccess.postValue(true)
    }

    fun networkErrorHandled() {
        _networkError.postValue(null)
    }

    fun updateNetworkState(connected: Boolean) {
        _networkConnected.postValue(connected)
    }

    fun flushErrors() {
        _networkError.value = null
    }

    //endregion

    //region venue data

    /**
     * Takes the location and figures if it should
     * call remote API or the cached data is fine
     */
    fun loadData(gpsLocation: GPSLocation) {
        repositoryScope.launch {
            if (userMoved(gpsLocation) && hasNetworkConnection()) {
                venueDAO.invalidateVenues()
            }

            if (shouldLoadFromRemoteSource(gpsLocation)) {
                loadRemoteData(gpsLocation)
            }

        }
    }

    /**
     * proxy for Remote API calls. checks the conditions
     * and decides if new data is needed
     */
    private suspend fun shouldLoadFromRemoteSource(gpsLocation: GPSLocation): Boolean {
        return (!hasDataInDB()
                || userMoved(gpsLocation))
                && !isLoadingSync
                && hasNetworkConnection()

    }

    /**
     * Actual data loading from remote source
     * @param gpsLocation to find places around
     * @param pageOffset for pagination
     */
    private fun loadRemoteData(gpsLocation: GPSLocation, pageOffset: Int = 0) {
        setLoading(true)
        repositoryScope.launch(exceptionHandler) {
            val data = remoteServices.getVenuesUserLess(
                clientId = BuildConfig.CLIENT_ID,
                clientSecret = BuildConfig.CLIENT_SECRET,
                limit = VENUE_REMOTE_API_PAGE_SIZE,
                version = BuildConfig.API_VERSION_QUERY,
                location = "${gpsLocation.latitude}, ${gpsLocation.longitude}",
                offset = pageOffset,
                sortByDistance = 1
            )
            insertVenues(data)
            saveUpdatedState(gpsLocation, data)
        }
    }

    /**
     * Gets called when user reaches bottom of the current
     * venue list. If there are missing venues, call remote
     * API to fetch them.
     */
    fun loadMore() {
        repositoryScope.launch {
            val countInDb = venueDAO.getVenueCount()
            if (countInDb < prevState.lastVenueCount && !isLoadingSync) {
                if (hasNetworkConnection()) {
                    loadRemoteData(prevState.lastGPSLocation, pageOffset = countInDb)
                } else {
                    _networkError.postValue(
                        Throwable("There is no internet connection available to load more data")
                    )
                }
            }
        }
    }

    /**
     * @return false if the database is empty
     */
    suspend fun hasDataInDB(): Boolean {
        return venueDAO.getVenueCount() != 0
    }

    /**
     * receives VenueResponse and transforms it
     * to database rows structure, before inserting into it.
     */
    private suspend fun insertVenues(data: VenueResponse) {
        venueDAO.insertVenues(data.response.groups.flatMap { groups ->
            groups.items.map { item ->
                var icon: String? = null
                item.venue.categories?.takeIf {
                    it.isNotEmpty()
                }?.apply {
                    icon = constructIconUrl(this[0])
                }
                Venue(item.venue, icon)
            }
        })
        setLoading(false)
    }

    /**
     * Updates the state shared preference preparing it
     * for next sessions or actions
     */
    private fun saveUpdatedState(
        gpsLocation: GPSLocation,
        data: VenueResponse
    ) {
        prevState = VenueAppState(
            gpsLocation,
            data.response.areaName,
            data.response.totalResults
        )
        prefsHelper.updateState(
            prevState
        )
    }

    private fun constructIconUrl(item: Category) =
        "${item.icon.path}${VENUE_ICON_SIZE}${item.icon.extension}"

    /**
     * Indicates if user movement should cause data
     * update by comparing it to [MINIMUM_UPDATE_DISTANCE]
     */
    private fun userMoved(gpsLocation: GPSLocation) =
        gpsLocation - prevState.lastGPSLocation > MINIMUM_UPDATE_DISTANCE

    private fun setLoading(loading: Boolean) {
        isLoadingSync = loading
        _isLoading.postValue(loading)
    }

    private fun hasNetworkConnection(): Boolean {
        return networkConnected.value!!
    }

    /**
     * @see io.mns.divsquare.data.cache.daos.VenueDAO#getVenueById
     */
    suspend fun getVenueById(id: String): Venue {
        return venueDAO.getVenueById(id)
    }

    fun areaName(): String {
        return prevState.lastKnownAreaName
    }
    //endregion

    /**
     * persists theme preference
     */
    fun saveTheme(isDark: Boolean) {
        prefsHelper.updateTheme(isDark)
    }

}