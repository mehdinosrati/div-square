package io.mns.divsquare.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import io.mns.divsquare.data.model.Venue
import io.mns.divsquare.databinding.ItemVenueBinding
import io.mns.divsquare.ui.adapters.callbacks.VenueClickCallBack
import io.mns.divsquare.ui.adapters.diffutil.VenuesDiffCallBack

/**
 * Turns Venue objects into see-able views.
 */
class VenueAdapter(private val callBack: VenueClickCallBack) :
    ListAdapter<Venue, VenueAdapter.VenueViewHolder>(VenuesDiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder {
        return VenueViewHolder.of(LayoutInflater.from(parent.context), parent, callBack)
    }

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id.hashCode().toLong()
    }

    class VenueViewHolder(
        private val binding: ItemVenueBinding,
        private val callBack: VenueClickCallBack
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(venue: Venue) {
            binding.venue = venue
            binding.callBack = callBack
            binding.executePendingBindings()
        }

        companion object {
            fun of(
                inflater: LayoutInflater,
                parent: ViewGroup,
                callBack: VenueClickCallBack
            ): VenueViewHolder {
                return VenueViewHolder(
                    ItemVenueBinding.inflate(
                        inflater,
                        parent,
                        false
                    ), callBack
                )
            }
        }
    }
}