package io.mns.divsquare.ui.viewModels

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import io.mns.divsquare.data.VenuesRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class MainViewModel(application: Application): AndroidViewModel(application), KoinComponent {
    private val repository: VenuesRepository by inject()
    var screenBitmap: Bitmap? = null


    fun hasData(): LiveData<Boolean> {
        return liveData {
            emit(repository.hasDataInDB())
        }
    }

    fun updateNetworkState(isConnected: Boolean) {
        repository.updateNetworkState(isConnected)
    }

    fun locationAccessGranted() {
        repository.locationAccessGranted()
    }

    fun flushErrors() {
        repository.flushErrors()
    }

    fun saveTheme(isDark: Boolean) {
        repository.saveTheme(isDark)
    }
}