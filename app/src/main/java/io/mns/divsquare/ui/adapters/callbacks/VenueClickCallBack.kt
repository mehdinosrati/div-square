package io.mns.divsquare.ui.adapters.callbacks

import io.mns.divsquare.data.model.Venue

/**
 * click callback for venue list item, in order
 * to move its logic handling out of the adapter
 */
interface VenueClickCallBack {
    fun venueClicked(venue: Venue)
}