package io.mns.divsquare.ui

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import io.mns.divsquare.R
import io.mns.divsquare.databinding.ActivityMainBinding
import io.mns.divsquare.ui.viewModels.MainViewModel
import io.mns.divsquare.utils.ui.createLocationErrorDialog
import io.mns.divsquare.utils.ui.isDark
import io.mns.divsquare.utils.ui.themeChanged
import io.mns.divsquare.utils.ui.toggleTheme

class MainActivity : BaseLocationActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModels<MainViewModel>()
    private var errorDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        handleRecreateAnimation()
    }

    /**
     * Location access is facing troubles.
     * If there is no cached data, user should see an error dialog
     */
    override fun locationAccessDenied() {
        viewModel.hasData().observe(this, Observer {
            if (!it) {
                errorDialog?.cancel()
                errorDialog = createLocationErrorDialog(
                    getString(R.string.error_title),
                    getString(R.string.load_fail_error_body)
                )
                errorDialog!!.show()
            }
        })
    }

    /**
     * Since [AppCompatDelegate.setDefaultNightMode] causes the activity
     * recreation, in order to be able to fool the user about changing
     * theme with animation, the animation should be handled inside
     * the newly created activity. Also place the bitmap of screen
     * inside the viewModel to prevent leaks and issues.
     */
    private fun handleRecreateAnimation() {
        if (viewModel.screenBitmap == null) {
            turnOnLocation()
            checkLocation()
        } else {
            themeChanged(viewModel.screenBitmap!!, binding.screen, binding.container)
        }
    }

    /**
     * Draws a screen painting in a bitmap object and puts
     * it in viewModel, before changing the theme
     */
    fun toggleTheme() {
        if (binding.screen.isVisible) {
            return
        }

        val w = binding.container.measuredWidth
        val h = binding.container.measuredHeight
        viewModel.screenBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(viewModel.screenBitmap!!)
        binding.container.draw(canvas)
        viewModel.saveTheme(!resources.isDark())
        resources.toggleTheme()
    }

    fun tryAgain() {
        turnOnLocation()
        checkLocation()
    }

    override fun locationAccessGranted() {
        viewModel.locationAccessGranted()
        registerNetworkListener()
    }

    override fun updateNetworkState(connected: Boolean) {
        viewModel.updateNetworkState(connected)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.flushErrors()
    }
}
