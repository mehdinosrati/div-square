package io.mns.divsquare.ui.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.mns.divsquare.data.VenuesRepository
import io.mns.divsquare.data.model.Venue
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class VenueDetailsViewModel(application: Application) : AndroidViewModel(application),
    KoinComponent {
    private val repository: VenuesRepository by inject()
    private val _venue = MutableLiveData<Venue>()

    private val _directionsClicked = MutableLiveData(false)
    val directionsClicked: LiveData<Boolean>
        get() = _directionsClicked

    fun loadDetails(id: String): LiveData<Venue> {
        viewModelScope.launch {
            _venue.postValue(repository.getVenueById(id))
        }
        return _venue
    }

    fun showDirections() {
        _directionsClicked.postValue(true)
    }

    fun directionsClickHandled() {
        _directionsClicked.postValue(false)
    }
}