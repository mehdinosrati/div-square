package io.mns.divsquare.ui.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.mns.divsquare.MAP_BASE_URL
import io.mns.divsquare.R
import io.mns.divsquare.databinding.FragmentVenueDetailsBinding
import io.mns.divsquare.ui.viewModels.VenueDetailsViewModel
import io.mns.divsquare.utils.ui.toast


class VenueDetailsFragment :
    BaseFragment<FragmentVenueDetailsBinding>(R.layout.fragment_venue_details) {
    private val viewModel by viewModels<VenueDetailsViewModel>()
    private val args by navArgs<VenueDetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.viewModel = viewModel
        loadVenue()
        handleDirections()
        setToolbarNavListener()
    }

    /**
     * Retrieves the stored venue based on its id
     * and sets the result as binding variable
     */
    private fun loadVenue() {
        viewModel.loadDetails(args.venueId).observe(viewLifecycleOwner, Observer {
            binding.venue = it
        })
    }

    /**
     * Evaluates the possibility of opening the location
     * in a map application, and does it.
     */
    private fun handleDirections() {
        viewModel.directionsClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                viewModel.directionsClickHandled()
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("${MAP_BASE_URL}${binding.venue?.location?.latitude},${binding.venue?.location?.longitude}")
                )
                if (intent.resolveActivity(requireContext().packageManager) != null) {
                    startActivity(intent)
                } else {
                    toast(getString(R.string.map_directions_no_app_found))
                }
            }
        })
    }

    private fun setToolbarNavListener() {
        binding.toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

}
