package io.mns.divsquare.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.mns.divsquare.GPS_REQUEST
import io.mns.divsquare.LOCATION_REQUEST
import io.mns.divsquare.data.location.LocationUtils

/**
 * Base activity that handles location permission obtaining
 * and requesting gps to be turned on. Also updates data about network state
 */
abstract class BaseLocationActivity : AppCompatActivity() {
    private var isGPSEnabled = false
    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            runOnUiThread {
                updateNetworkState(true)
            }
        }

        override fun onLost(network: Network?) {
            runOnUiThread {
                updateNetworkState(false)
            }
        }

        override fun onUnavailable() {
            runOnUiThread {
                updateNetworkState(false)
            }
        }
    }
    private val networkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .build()

    abstract fun locationAccessDenied()

    abstract fun locationAccessGranted()

    abstract fun updateNetworkState(connected: Boolean)

    /**
     * Check if the permission is granted to access the location,
     * if it has not, request it, when it gets the permissions and
     * turns on the gps, notify MainActivity by calling [locationAccessGranted]
     */
    fun checkLocation() {
        if (isLocationAccessPermissionsGranted()) {
            if (isGPSEnabled) {
                locationAccessGranted()
            }
        } else {
            getLocationAccessPermission()
        }
    }

    fun turnOnLocation() {
        LocationUtils(this).turnGPSOn(object : LocationUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                this@BaseLocationActivity.isGPSEnabled = isGPSEnable
                if (isGPSEnable && isLocationAccessPermissionsGranted()) {
                    locationAccessGranted()
                }
            }
        })
    }

    protected fun registerNetworkListener() {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        @Suppress(
            "DEPRECATION",
            "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
        )
        // for launch time state detection
        if (connectivityManager.activeNetworkInfo == null ||
            !connectivityManager.activeNetworkInfo.isConnectedOrConnecting
        ) {
            updateNetworkState(false)
        } else {
            updateNetworkState(true)
        }

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)
    }

    private fun getLocationAccessPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            LOCATION_REQUEST
        )
    }

    private fun isLocationAccessPermissionsGranted() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    checkLocation()
                } else {
                    locationAccessDenied()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GPS_REQUEST) {
                isGPSEnabled = true
                checkLocation()
            }
        } else {
            locationAccessDenied()
        }
    }
}