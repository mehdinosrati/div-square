package io.mns.divsquare.ui.fragments

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import io.mns.divsquare.R
import io.mns.divsquare.data.model.GPSLocation
import io.mns.divsquare.data.model.Venue
import io.mns.divsquare.databinding.FragmentVenueListBinding
import io.mns.divsquare.ui.MainActivity
import io.mns.divsquare.ui.adapters.VenueAdapter
import io.mns.divsquare.ui.adapters.callbacks.VenueClickCallBack
import io.mns.divsquare.ui.viewModels.VenueListViewModel
import io.mns.divsquare.utils.ui.createNetworkErrorDialog


class VenueListFragment : BaseFragment<FragmentVenueListBinding>(R.layout.fragment_venue_list) {
    private val viewModel by viewModels<VenueListViewModel>()
    private lateinit var adapter: VenueAdapter
    private var activeDialog: Dialog? = null
    private fun loadMore() = viewModel.loadMore()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.viewModel = viewModel
        populateVenues()
        observeLocation()
        observeLoadingState()
        setThemeToggleButtonListener()
    }

    //region venues
    /**
     * Observes venues list in database and updates the adapter
     * and potentially the list, after initializing them
     */
    private fun populateVenues() {
        initializeAdapter()
        initializeRecyclerView()
        viewModel.venues().observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }

    private fun observeLoadingState() {
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.isLoading = it
        })
    }
    //endregion

    //region location
    /**
     * Checks if has obtained location access,
     * if it has, registers the observer for updates
     */
    private fun observeLocation() {
        // in order not to start observing before granting necessary permissions
        viewModel.hasLocationAccess().observe(viewLifecycleOwner, Observer {
            if (it) {
                // permissions are granted and now we can access user location
                binding.isLocating = true
                viewModel.locationLiveData.observe(viewLifecycleOwner, Observer { data ->
                    binding.isLocating = false
                    observeNetwork(data)
                    viewModel.loadData(
                        GPSLocation(
                            latitude = data.latitude,
                            longitude = data.longitude
                        )
                    )
                })
            }
        })
    }
    //endregion

    //region network issues
    /**
     * Checks network state and compares it with the cached data status
     * and shows an error dialog if one needs to be shown. Also if
     * the network goes on while the empty list is up, requests for data.
     * And observes network errors could possibly happen, shows error dialog respectively
     */
    private fun observeNetwork(gpsLocation: GPSLocation) {
        viewModel.networkStatus.observe(viewLifecycleOwner, Observer {
            if (!it) {
                viewModel.hasData().observe(viewLifecycleOwner, Observer { hasData ->
                    if (!hasData && activeDialog == null) {
                        handleNoInternetError()
                    }
                })
            } else {
                viewModel.hasData().observe(viewLifecycleOwner, Observer { hasData ->
                    if (!hasData) {
                        viewModel.loadData(gpsLocation)
                    }
                })
            }
        })

        viewModel.networkError.observe(viewLifecycleOwner, Observer {
            if (it?.message != null && activeDialog == null && !viewModel.hasShownLoadMoreError) {
                handleNetworkErrors(it)
            }
        })
    }

    /**
     * shows the error dialog about error
     */
    private fun handleNetworkErrors(throwable: Throwable) {
        viewModel.hasShownLoadMoreError = true
        activeDialog =
            createNetworkErrorDialog(getString(R.string.error_title), throwable.message.toString())
        activeDialog!!.setOnCancelListener {
            activeDialog?.cancel()
            activeDialog = null
        }
        activeDialog!!.show()
        viewModel.networkErrorHandled()
    }

    private fun handleNoInternetError() {
        activeDialog = createNetworkErrorDialog(
            getString(R.string.error_title),
            getString(R.string.no_cached_data_network_error_text)
        )
        activeDialog!!.setOnCancelListener {
            activeDialog?.cancel()
            activeDialog = null
        }
        activeDialog!!.show()
        viewModel.networkErrorHandled()
    }
    //endregion

    //region list ui
    private fun initializeAdapter() {
        adapter = VenueAdapter(object : VenueClickCallBack {
            override fun venueClicked(venue: Venue) {
                val action =
                    VenueListFragmentDirections.actionVenueListFragmentToVenueDetailsFragment(venue.id)
                findNavController().navigate(action)
            }
        })
        adapter.setHasStableIds(true)
    }

    private fun initializeRecyclerView() {
        binding.venueList.loadMoreAction = ::loadMore
        binding.venueList.adapter = adapter
    }
    //endregion

    private fun setThemeToggleButtonListener() {
        binding.toolbar.setNavigationOnClickListener {
            (requireActivity() as MainActivity).toggleTheme()
        }
    }
}
