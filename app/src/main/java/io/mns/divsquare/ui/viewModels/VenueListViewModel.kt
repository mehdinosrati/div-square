package io.mns.divsquare.ui.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import io.mns.divsquare.data.VenuesRepository
import io.mns.divsquare.data.location.LocationLiveData
import io.mns.divsquare.data.model.GPSLocation
import org.koin.core.KoinComponent
import org.koin.core.inject

class VenueListViewModel(application: Application) : AndroidViewModel(application), KoinComponent {
    private val repository: VenuesRepository by inject()
    val locationLiveData: LocationLiveData by inject()
    val isLoading = repository.isLoading
    val areaName = repository.areaName()
    val networkStatus = repository.networkConnected
    val networkError = repository.networkError
    var hasShownLoadMoreError = false

    fun hasLocationAccess() = repository.hasLocationAccess

    fun venues() = repository.venues

    fun networkErrorHandled() {
        repository.networkErrorHandled()
    }

    fun hasData() = liveData {
        emit(repository.hasDataInDB())
    }

    fun loadData(gpsLocation: GPSLocation) {
        repository.loadData(gpsLocation)
    }

    fun loadMore() {
        repository.loadMore()
    }
}