package io.mns.divsquare.ui.adapters.diffutil

import androidx.recyclerview.widget.DiffUtil
import io.mns.divsquare.data.model.Venue

/**
 * Helps to update adapter items in a smart manner
 */
class VenuesDiffCallBack : DiffUtil.ItemCallback<Venue>() {
    override fun areItemsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Venue, newItem: Venue): Boolean {
        return oldItem == newItem
    }
}