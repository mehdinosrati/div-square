package io.mns.divsquare

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import io.mns.divsquare.di.appModule
import io.mns.divsquare.utils.SharedPreferencesHelper
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    private val prefsHelper by inject<SharedPreferencesHelper>()

    override fun onCreate() {
        super.onCreate()
        initKoin()
        applyTheme()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(appModule)
        }
    }

    /**
     * Set theme based on user's last preference
     */
    private fun applyTheme() {
        if (prefsHelper.isDarkTheme()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }
}