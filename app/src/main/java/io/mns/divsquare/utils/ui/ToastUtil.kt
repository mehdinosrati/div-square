package io.mns.divsquare.utils.ui

import android.widget.Toast
import androidx.fragment.app.Fragment

/**
 * centralized toast util, making it possible to change
 * all Toast styles easily at any point in future
 */
fun Fragment.toast(text: String) {
    Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
}