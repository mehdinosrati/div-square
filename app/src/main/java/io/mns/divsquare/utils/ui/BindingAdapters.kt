package io.mns.divsquare.utils.ui

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.mns.divsquare.R

@BindingAdapter("isVisible")
fun isVisible(v: View, isVisible: Boolean) {
    v.isVisible = isVisible
}

@BindingAdapter("loadImage")
fun loadImage(imageView: ImageView, url: String?) {
    Glide.with(imageView).load(url)
        .apply(
            RequestOptions().error(R.drawable.ic_default_icon)
                .placeholder(R.drawable.ic_default_icon)
        )
        .into(imageView)
}

@BindingAdapter("blinkPeriod")
fun animateBlink(v: View, period: Int) {
    val anim = AlphaAnimation(1f, 0f)
    anim.apply {
        duration = period.toLong()
        startOffset = 300
        repeatMode = Animation.REVERSE
        repeatCount = Animation.INFINITE
    }
    v.startAnimation(anim)
}