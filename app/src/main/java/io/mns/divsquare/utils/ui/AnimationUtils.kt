package io.mns.divsquare.utils.ui

import android.animation.Animator
import android.animation.ValueAnimator
import android.graphics.Color
import android.view.animation.Interpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.animation.PathInterpolatorCompat

/**
 * Makes on-liner ValueAnimator
 */
inline fun <reified T> ValueAnimator.animatedValue(crossinline update: (T) -> Unit): ValueAnimator {
    addUpdateListener { update(it.animatedValue as T) }
    return this
}

/**
 * In order to prevent sudden color change on theme switch
 */
fun AppCompatActivity.animateStatusBarForTheme(): Animator {
    return if (resources.isDark()) {
        ValueAnimator.ofArgb(Color.parseColor("#CCCCCC"), Color.parseColor("#222222"))
            .animatedValue(window::setStatusBarColor)
    } else {
        ValueAnimator.ofArgb(Color.parseColor("#222222"), Color.parseColor("#CCCCCC"))
            .animatedValue(window::setStatusBarColor)
    }
}

/**
 * To match telegram style of animating theme change
 */
val quadEaseInOutInterpolator: Interpolator = PathInterpolatorCompat.create(.455f, .03f, .515f, .955f)