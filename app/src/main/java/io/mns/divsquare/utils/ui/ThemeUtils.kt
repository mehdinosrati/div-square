package io.mns.divsquare.utils.ui

import android.animation.AnimatorSet
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.os.Handler
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.animation.doOnEnd
import androidx.core.view.isVisible
import io.mns.divsquare.R
import io.mns.divsquare.ui.MainActivity
import kotlin.math.hypot

fun Resources.toggleTheme() {
    if (isDark()) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    } else {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }
}

fun Resources.isDark() =
    (configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) != Configuration.UI_MODE_NIGHT_NO

fun MainActivity.themeChanged(bitmap: Bitmap, imageView: ImageView, container: ViewGroup) {
    Handler().postDelayed(
        {
            try {
                imageView.setImageBitmap(bitmap)
                imageView.isVisible = true
                val w = container.measuredWidth
                val h = container.measuredHeight
                val finalRadius = hypot(w.toFloat(), h.toFloat())
                val cx = resources.getDimensionPixelSize(R.dimen.moon_left)
                val cy = resources.getDimensionPixelSize(R.dimen.moon_top)
                val anim = ViewAnimationUtils.createCircularReveal(
                    container,
                    cx,
                    cy,
                    0f,
                    finalRadius
                )
                anim.duration = 600L
                anim.interpolator = quadEaseInOutInterpolator
                anim.doOnEnd {
                    imageView.setImageDrawable(null)
                    imageView.isVisible = false
                }
                AnimatorSet().apply {
                    playTogether(
                        animateStatusBarForTheme(),
                        anim
                    )
                    start()
                }
            } catch (e: Exception) {
                imageView.isVisible = false
                imageView.setImageBitmap(null)
            }
        },
        10
    )
}