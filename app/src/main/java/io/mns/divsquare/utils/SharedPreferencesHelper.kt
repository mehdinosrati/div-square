package io.mns.divsquare.utils

import android.content.Context
import androidx.core.content.edit
import io.mns.divsquare.*
import io.mns.divsquare.data.model.GPSLocation
import io.mns.divsquare.data.model.VenueAppState

/**
 * Helper class to read and write from shared preferences with less pain
 */
class SharedPreferencesHelper(private var appContext: Context) {
    private val statePreferences = appContext.getSharedPreferences(APP_STATE_PREFS, Context.MODE_PRIVATE)
    private val themePreferences = appContext.getSharedPreferences(THEME_PREFERENCES, Context.MODE_PRIVATE)
    fun getLastState(): VenueAppState {
        val lastLocation: String =
            statePreferences.getString(LAST_LOADED_LOCATION_PREF_KEY, "0.0, 0.0")!!
        val lastAreaName: String = statePreferences.getString(
            LAST_LOADED_AREA_NAME_PREF_KEY, appContext.resources.getString(
                R.string.app_name
            )
        )!!
        val lastTotalVenues = statePreferences.getInt(LAST_TOTAL_VENUES_COUNT_PREF_KEY, 0)
        val coordinates = lastLocation.split(", ")
        val location =
            GPSLocation(latitude = coordinates[0].toDouble(), longitude = coordinates[1].toDouble())
        return VenueAppState(location, lastAreaName, lastTotalVenues)
    }

    fun updateState(appState: VenueAppState) {
        val coordinates =
            "${appState.lastGPSLocation.latitude}, ${appState.lastGPSLocation.longitude}"
        statePreferences.edit {
            putString(LAST_LOADED_LOCATION_PREF_KEY, coordinates)
            putString(LAST_LOADED_AREA_NAME_PREF_KEY, appState.lastKnownAreaName)
            putInt(LAST_TOTAL_VENUES_COUNT_PREF_KEY, appState.lastVenueCount)
        }
    }

    fun isDarkTheme(): Boolean {
        return themePreferences.getBoolean(IS_DARK_THEME_PREFS_KEY, false)
    }

    fun updateTheme(isDark: Boolean) {
        themePreferences.edit {
            putBoolean(IS_DARK_THEME_PREFS_KEY, isDark)
        }
    }
}