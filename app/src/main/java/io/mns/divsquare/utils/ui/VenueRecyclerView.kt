package io.mns.divsquare.utils.ui

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.mns.divsquare.VENUE_REMOTE_API_PAGE_SIZE

/**
 * [RecyclerView] child, encapsulates stuff about venues list
 * to make the interface cleaner
 */
class VenueRecyclerView : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )
    var loadMoreAction: (() -> Unit)? = null
    private val itemDecorator = DividerItemDecoration(
        context,
        LinearLayoutManager.VERTICAL
    )

    /**
     * handles pagination
     */
    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = (layoutManager as LinearLayoutManager)
            val visibleItemCount: Int = layoutManager.childCount
            val totalItemCount: Int = layoutManager.itemCount
            val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                firstVisibleItemPosition >= 0 &&
                totalItemCount >= VENUE_REMOTE_API_PAGE_SIZE
            ) {
                loadMoreAction?.let {
                    it()
                }
            }

        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addOnScrollListener(scrollListener)
        setHasFixedSize(true)
        addItemDecoration(itemDecorator)
    }
}