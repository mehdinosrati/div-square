package io.mns.divsquare.utils.ui

import android.app.Dialog
import android.view.ContextThemeWrapper
import androidx.appcompat.app.AlertDialog
import io.mns.divsquare.R
import io.mns.divsquare.ui.MainActivity
import io.mns.divsquare.ui.fragments.VenueListFragment

fun MainActivity.createLocationErrorDialog(title: String, text: String): Dialog {
    val builder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.dialogTheme))
    builder.setTitle(title)
    builder.setMessage(text)
    builder.setCancelable(false)
    builder.setIcon(R.drawable.ic_error)
    builder.setPositiveButton("Try Again") { dialog, _ ->
        dialog.cancel()
        tryAgain()
    }

    builder.setNegativeButton("Exit") { _, _ ->
        finish()
    }

    return builder.create()
}

fun VenueListFragment.createNetworkErrorDialog(title: String, text: String): Dialog {
    val builder = AlertDialog.Builder(ContextThemeWrapper(requireContext(), R.style.dialogTheme))
    builder.setTitle(title)
    builder.setMessage(text)
    builder.setCancelable(true)
    builder.setIcon(R.drawable.ic_error)
    builder.setPositiveButton("Ok") { dialog, _ ->
        dialog.cancel()
    }
    return builder.create()
}