package io.mns.divsquare.di

import androidx.room.Room
import io.mns.divsquare.BuildConfig
import io.mns.divsquare.VENUE_DATABASE_NAME
import io.mns.divsquare.data.VenuesRepository
import io.mns.divsquare.data.cache.VenueDataBase
import io.mns.divsquare.data.location.LocationLiveData
import io.mns.divsquare.data.remote.RemoteServices
import io.mns.divsquare.utils.SharedPreferencesHelper
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Koin dependency structure definition for
 * application module, which is the only one
 */
val appModule = module {
    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RemoteServices::class.java)
    }

    single {
        Room.databaseBuilder(
            get(),
            VenueDataBase::class.java,
            VENUE_DATABASE_NAME)
            .build()
    }

    single {
        LocationLiveData(get())
    }

    single {
        SharedPreferencesHelper(get())
    }

    single {
        VenuesRepository(get(), get<VenueDataBase>().venueDao(), get())
    }
}